---
title: "How we built Status Page"
author: "Olena Horal-Koretska"
author_gitlab: ohoral
author_twitter: sharlatta
categories: unfiltered
image_title: "/images/blogimages/status-page/red-green-chilli.jpg"
description: "Insider Insights"
tags: DevOps
cta_button_text: "Give it a try!"
cta_button_link: "https://docs.gitlab.com/ee/user/project/status_page/"
twitter_text: "Building Status Page: May all your systems be operational"
---

{::options parse_block_html="true" /}

<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>&nbsp;&nbsp;
This blog post is [Unfiltered](/handbook/marketing/blog/unfiltered/#legal-disclaimer)
&nbsp;&nbsp;<i class="fab fa-gitlab" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
{: .alert .alert-webcast}

### Introduction

Status Page is a tool for communicating incidents status and maintenance times. Recently we started building this tool at GitLab to provide the best incident management experience both for our internal team and our customers.

###  Current Status Update Approach

Incident handling in GitLab happens inside the issue in a dedicated public project. The team discusses and posts updates there. Public updates are manually published by engineer-on-call to [status.gitlab.com](https://status.gitlab.com/) every 15 mins. This is not ideal - responders lose precious time during fire-fight switching tools and duplicating information. Also having public project for incident management means:

1. massive load on your instance in the "hard times"
2. higher monetary cost
3. no access to status updates if your GitLab instance is down
4. sensitive information that comes up in a discussion is public and may cause vulnerability exploit while it is being fixed


### Requirements
Our first customer was our internal team. We [dogfood everything](https://about.gitlab.com/handbook/engineering/#dogfooding), and the Status Page wasn't an exception. So requirements were built based on internal team needs:

1. **No tool switching for incidents updates:** people that handle incidents have enough burden on their shoulders with fixing incidents. So we should spare them responding to pings about what happened, what the status of the incident is, and how it is progressing. But at the same time provide to those impatient once updates as soon as they are available. Incident Status should be updated in one place both for peer-problem-solvers and the public.

1. **Ability to control visibility level - which updates are published and which are not**: when you have an issue in your product you do not necessarily want to shout out: "Hey, you malicious hacker, we've got a problem - go exploit it". You want to let your team handle it calmly in a timely manner. But at the same time you want to send assuasive messages to the public without distracting fire-fight team.

1. **Display all types of data from GitLab incident description and comments: markdown, images, embedded charts on Status Page.** As incidents are handled in GitLab issues, a variety of data representation is available to showcase and communicate problem or solution. This rich data has to be available in public updates.

## Building Status Page
Our new Status Page is designed to address all above mentioned concerns.

#### The SPIKE

*[SPIKE](https://about.gitlab.com/handbook/engineering/development/ops/monitor/#spike): an investigation of available solutions and risk estimation. It is necessary so the team can be aligned on general direction before starting the implementation itself*

Initially, we considered leveraging one of many open-source Status Page implementations. But none of them could satisfy all our requirements, so eventually we decided to just go ahead and build our own implementation.

#### Backend and Data Scraping

 When we started, we first brainstormed all the different solutions we could utilize to collect data from incidents issues to be automatically published to the Status Page:

**Option 1: (GitLab) Webhooks - user sets up the endpoint to which GitLab will post incident updates**
![Webhook](/images/blogimages/status-page/webhook.png){: .center}

**Option 2: Alerts coming directly from Prometheus Alertmanager**
![ALerts](/images/blogimages/status-page/alerts.png){: .center}

**Option 3: Status page itself monitoring other services**
![Monitoring](/images/blogimages/status-page/monitoring.png){: .center}

**Option 4: Human beings pushing a markdown file to git or calling the API with some utility. e.g. `curl`**
![Git Commit](/images/blogimages/status-page/gitcommit.png){: .center}

**Option 5: CI job running manually or scheduled to run during certain intervals**
![CI Job](/images/blogimages/status-page/cijob.png){: .center}

Those approaches required either manual user input, additional CI resources, or building some sophisticated piece of software that was unnecessary for this case. So refining the list down to the optimal solution, without over-engineering while being able to provide instant feedback on Status Page, the incident issue is converted to JSON and published to the Status Page by a background job.

#### Frontend

Here at GitLab we love VueJS so much we contribute to it, and so the team has great expertise. Consequently our component library [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui/) and styling utilities are based on VueJS. You could have guessed that we didn't have much doubt about the frontend framework to use! Besides the UI library as a dependency, GitLab provides `eslint`, `stylelint`, and SVGs as npm packages as well. It was very convenient to have them handy as any new project setup always raises lots of questions about best practices & best tools. With all of this, the Status Page was able to be GitLab-branded. Feel free to use GitLab utilities in your own project too!

NOTE: Status Page is a stand-alone application, hosted in a separate GitLab repository consuming JSON files generated by a background job. It is distributed under MIT license and can be used apart from GitLab given that correct data source is provisioned. At the same time, you'll get the best experience by using our status page with GitLab.

Frontend along with generated JSON data sources is published to [cloud storage](https://www.youtube.com/watch?v=27GgP6BXR6A) - currently we support only Amazon S3 because we are hosted on Google Cloud and want our status page to still be available if Google Cloud is down. Credentials are provided by the user when setting up incident tracking project for Status Page.

#### The Solution

Once an incident issue is created/updated in GitLab (manually or via [alert](https://docs.gitlab.com/ee/user/incident_management/#incident-management)), its description (with all types of data) along with comments that were marked as public will be picked by background job, converted to JSON and mirrored on Status Page.

![Status Page flow](/images/blogimages/status-page/status-page-flow.png){: .center}

### Give it a try
Here's a great [step by step guide](https://docs.gitlab.com/ee/user/project/status_page/) on how to setup a Status Page for your project with GitLab. Enjoy and may all your systems be operational!

### Collaboration
I do not want to tire you, my dear reader,  with more technical details. There was much more discussed and still to be implemented. But all this wouldn't be possible without great collaboration from the  [Monitor:Health Team](https://about.gitlab.com/handbook/engineering/development/ops/monitor/health) to whom I'm thankful for all heated discussions, great insights, quick iterations, fast fails and advantage to work with.

Cover image by [Melina Yakas](https://unsplash.com/@myakas16) on [Unsplash](https://unsplash.com/photos/OBWEXPOurWo)
{: .note}