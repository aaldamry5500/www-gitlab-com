---
layout: markdown_page
title: "Category Direction - Web Application Firewall"
---

- TOC
{:toc}

## Description
A web application firewall (WAF) filters, monitors, and blocks web traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications, while regular firewalls serve as a safety gate between servers. By inspecting the contents of web traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

GitLab's WAF is based on the open-source ModSecurity project using the OWASP Top 10 ruleset.

### Goal

GitLab's goal with WAF is to provide visibility into your applications, clusters, and the traffic they receive. By being able to see what is being sent to your systems, GitLab wants to empower you to log and/or block malicious traffic based on configuration.  This also allows you have awareness of how your application is being attacked so you can determine what actions to take in order to reduce the chances of those attacks being successful.
 
Additionally, we want to make it possible to identify and update parts of your app that are subject to malicious traffic. In this way, even if malicious traffic bypasses the WAF, you can ensure the underlying application itself is resilient against attacks like SQL injection or XSS.

### Roadmap
[Planned to Viable](https://gitlab.com/groups/gitlab-org/-/epics/1650)

## What's Next & Why
In 12.10 we are [allowing logs to be exported](https://gitlab.com/gitlab-org/gitlab/-/issues/199268) to an external SIEM or central logging system to make it easy to see what the WAF is doing.  Additionally we are extending the controls we added to also allow for an easy way to [switch the WAF between logging and blocking mode](https://gitlab.com/gitlab-org/gitlab/-/issues/207173).

As we have achieved [Minimal](https://about.gitlab.com/direction/maturity/) maturity, our next steps are to temporarily pause additional development in this area to consolidate our focus on the Container Network Security and Container Behavior Analytics categories to move their maturity forward.

## Competitive Landscape
Some other vendors in the WAF market include the following:

- [Akamai](https://www.akamai.com/us/en/resources/application-firewall.jsp)
- [Alibaba Cloud](https://www.alibabacloud.com/product/waf)
- [Amazon Web Services](https://aws.amazon.com/waf/)
- [Barracuda Networks](https://www.barracuda.com/products/webapplicationfirewall)
- [Cloudflare](https://www.cloudflare.com/waf/)
- [F5 Networks](https://www.f5.com/products/security/advanced-waf)
- [Fortinet](https://www.fortinet.com/products/web-application-firewall/fortiweb.html)
- [Imperva](https://www.imperva.com/products/web-application-firewall-waf/)
- [Microsoft](https://docs.microsoft.com/en-us/azure/application-gateway/waf-overview)
- [Oracle](https://www.oracle.com/cloud/security/cloud-services/web-application-firewall.html)
- [Radware](https://www.radware.com/products/appwall/)
- [Signal Sciences](https://www.signalsciences.com/)

## Analyst Landscape
Gartner provides a Magic Quadrant for the [Web Application Firewall](https://www.gartner.com/en/documents/3964460) market.

## Top Customer Success/Sales Issue(s)
As this category is new, we do not yet have top customer success stories or sales issues.

## Top Customer Issue(s)
The category is very new, so we still need to engage customers and get feedback about their interests and priorities in this area.

## Top Vision Item(s)
TODO
